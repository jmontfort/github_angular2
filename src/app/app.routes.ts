import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { HomeComponent } from '../app/components/home/home.component';
import { RepositoryComponent } from '../app/components/repository/repository.component';

const appRoutes: Routes = [
    {path: '', component: HomeComponent, pathMatch: 'full'},
    {path: 'repository/:userName/:repoName', component: RepositoryComponent}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);   