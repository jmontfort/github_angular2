import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';

import { Repository } from '../classes/Repository';
import { Commit } from '../classes/Commit';

@Injectable()
export class GithubService {
  apiUrl: string;
  repositoriesApiUrl: string;
  repositories: Repository[] = [];
  commits: Commit[] = [];

  constructor(private _http:Http, private router:Router) { 
  }


  search(str: string){
    let headers = new Headers();
    headers.append('Content-type', 'application/json');
    return this._http.get("https://api.github.com/search/repositories?q="+str+"&sort=stars&order=desc&page=1", { headers: headers, withCredentials: false })
        .map(this.extractRepo)
        .catch(error => this.handleError(error, this.router));
  }

  getCommits(params: any){
    let headers = new Headers();
    headers.append('Content-type', 'application/json');
    return this._http.get("https://api.github.com/repos/"+ params.userName +"/" + params.repoName+ "/commits", 
      { headers: headers, withCredentials: false })
        .map(this.extractRepo)
        .catch(error => this.handleError(error, this.router));
    
  }

   private extractRepo(res: Response): Repository {
    let repositories = res.json();
    return repositories || { };
  }

   private extractCommits(res: Response): Commit {
    let commits = res.json();
    return commits || { };
  }

     private handleError (error: any, router:Router) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    return Observable.throw(errMsg);
  }


}
