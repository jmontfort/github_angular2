import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import { GithubService } from '../../services/github.service';
import { Repository } from '../../classes/Repository';
import { Commit } from '../../classes/Commit';

@Component({
  selector: 'app-repository',
  templateUrl: './repository.component.html',
  styleUrls: ['./repository.component.css']
})
export class RepositoryComponent implements OnInit {
  userName: string;
  repoName: string;
  commits: Commit[]=[];
  repositoryOwner: string;
  params: any;
  constructor(private _githubService:GithubService, private _route:ActivatedRoute) { }

  ngOnInit() {
    let {params} = this._route.snapshot;
    this._githubService.getCommits(params)
    .subscribe(commits => {
      this.commits = commits;
      this.params = params;
      this.repositoryOwner = this.params.userName + "/" + this.params.repoName;
      console.log(this.commits);
    })
    }
  }


