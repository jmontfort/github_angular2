import { Component, OnInit } from '@angular/core';

import { GithubService } from '../../services/github.service'
import { Repository } from '../../classes/Repository'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  searchStr: string;
  repositories: Repository[] = [];

  constructor(private _githubService:GithubService) { }


  ngOnInit() {
    if(localStorage.getItem('searchStr') != "")
      this.searchStr = localStorage.getItem('searchStr');
      this.searchRepos();
  }

  searchRepos() {
    this._githubService.search(this.searchStr)
      .subscribe(res => {
        this.repositories = res.items;
        localStorage.setItem('searchStr', this.searchStr);
    })
  }

}
