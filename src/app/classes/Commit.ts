import { Injectable } from '@angular/core';

import { Author } from '../classes/Author';
import { Committer } from '../classes/Committer';
import { Tree } from '../classes/Tree';

@Injectable()
export class Commit {
    message: string;
    url: string;
    comment_count: number;
    author:Author;
    committer: Committer;
    tree: Tree;
}        
