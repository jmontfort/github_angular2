import { Injectable } from '@angular/core';

import { Owner } from 'app/classes/Owner'

@Injectable()
export class Repository {
    login: Owner[] = [];
    name: string;
    html_url: string;
    craeted_at: string;
    pushed_at: string;
}