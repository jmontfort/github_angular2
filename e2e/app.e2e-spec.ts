import { GithubAngular2Page } from './app.po';

describe('github-angular2 App', function() {
  let page: GithubAngular2Page;

  beforeEach(() => {
    page = new GithubAngular2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
